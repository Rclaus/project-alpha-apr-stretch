from django.shortcuts import render, get_object_or_404, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import ProjectForm


# Create your views here.
@login_required
def list_projects(request):
    lists = Project.objects.filter(owner=request.user)
    context = {
        "project_list": lists,
    }
    return render(request, "projects/projects.html", context)


@login_required
def show_project(request, id):
    detail = get_object_or_404(Project, id=id)
    tasks = detail.tasks.all()
    context = {
        "project_detail": detail,
        "tasks": tasks,
    }
    return render(request, "projects/projectdetail.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save(False)
            project.owner = request.user
            project.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()

    context = {
        "form": form,
    }

    return render(request, "projects/create.html", context)
